<UNITS>      ft
<INTEG_AXIS> X
<SYMM>       y
<STRAKES>    6

<STA_COORD> 160.94 # Station 5.689
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.94  0.00   42.94  0.0    42.94  0.0    44.0   0.0  
# WL H      W      H  MD  W
    48.0   0.0    50.38  0.0  

<STA_COORD> 160.94 # Station 5.689
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.94  0.00   42.94 13.24   42.94 13.24   44.0  13.24 
# WL H      W      H  MD  W
    48.0  13.24   50.38  13.24 

<STA_COORD> 169.8  # Station 6
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.80  0.00   42.80 22.006  42.80 22.006  44.0  22.006
# WL H      W      H  MD  W
    48.0  22.006  50.26 22.006

<STA_COORD> 190.0  # Station 6.714
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.53  0.00   42.53 41.99   42.53 41.99   44.0  41.99
# WL H      W      H  MD  W
    48.0  41.99   49.95 41.99

<STA_COORD> 190.0  # Station 6.714
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.53  0.00   42.53 41.99   42.53 41.99   44.0  42.04
# WL H      W      H  MD  W
    48.0  42.25   49.95 42.22

<STA_COORD> 198.1  # Station 7
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.42  0.00   42.42 42.21   42.42 42.21   44.0  42.24
# WL H      W      H  MD  W
    48.0  42.50   49.82 42.50

<STA_COORD> 226.4  # Station 8
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    42.01  0.00   42.01 43.12   42.01 43.12   44.0  43.17
# WL H      W      H  MD  W
    48.0  43.45  49.32 43.45

<STA_COORD> 254.7  # Station 9
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    41.56  0.00   41.56 43.41   41.56 43.41   44.0  43.43
# WL H      W      H  MD  W
    48.0  43.90   49.00 43.90

<STA_COORD> 283.0  # Station 10
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    41.25  0.00   41.25 44.79   41.25 44.79   44.0  44.00
# WL H      W      H  MD  W
    48.0  44.00   48.65 44.00

<STA_COORD> 311.3  # Station 11
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    39.23  0.00   39.23 45.26   40.0  45.15   44.0  44.00
# WL H      W      H  MD  W
    48.0  44.00   48.20 44.00

<STA_COORD> 314.0  # Station 11.095
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    39.19  0.00   39.19 45.27   40.0  45.15   44.0  44.08
# WL H      W      H  MD  W
    47.99 44.067  48.17 44.067

<STA_COORD> 314.0  # Station 11.095
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    39.19  0.00   39.19 44.067  40.0  44.067  44.0  44.067
# WL H      W      H  MD  W
    47.99 44.067  48.17 44.067

<STA_COORD> 334.25 # Station 11.837
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    38.895 0.00  38.895  9.279  40.0   9.279  44.0   9.279
# WL H      W      H  MD  W
    47.91  9.279  47.94  9.279

<STA_COORD> 334.25 # Station 11.837
<STK_COORDS> 
# WL H      W      H      W      H      W      H      W
    38.895 0.00  38.895  0.0    40.0   0.0    44.0   0.0 
# WL H      W      H  MD  W
    47.91  0.0    47.94  0.0  
<EOF> 

