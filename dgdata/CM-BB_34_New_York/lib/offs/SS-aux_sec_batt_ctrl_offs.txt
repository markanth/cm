<UNITS>      ft
<INTEG_AXIS> Z
<SYMM>       n
<STRAKES>    5

<STA_COORD>  -12.5  # Stbd closure
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0 282.    88.0  282.    88.0  282.    79.0  282.     79.0 282.

<STA_COORD>  -12.5  # Stbd end
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0 282.    88.0  282.    88.0  290.    79.0  290.     79.0  282.  

<STA_COORD>   -2.5  # Stbd middle
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0  279.    88.0   279.0  88.0  293.    79.0  293.    79.0  279.0    

<STA_COORD>    2.5  # Port middle
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0  279.0  88.0   279.0   88.0  293.    79.0  293.    79.0   279.0   

<STA_COORD>   12.5  # Port end
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0  282.   88.0   282.   88.0   290.    79.0   290.   79.0   282.

<STA_COORD>   12.5  # Port closure
<STK_COORDS> 
#    H      L      H      L      H      L      H      L      H      L
    79.0   282.   88.0  282.    88.0   282.    79.0  282.   79.0  282.  
<EOF> 
