<UNITS>      ft
<INTEG_AXIS> X
<SYMM>       y
<STRAKES>    21

<STA_COORD>  0.0   # Station 0-CL
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    2.76  0.0     2.76  0.0     2.76  0.0     3.0   0.0     4.0   0.0   
# WL H      W      H      W      H      W      H      W      H      W
    5.0   0.0     6.0   0.0     8.0   0.0     10.0  0.0     12.0  0.0   
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  0.0     20.0  0.0     22.28 0.0     26.0  0.0     28.36 0.0   
# WL H      W      H      W      H      W      H      W      H      W
    34.0  0.00    37.1  0.00    40.0  0.00    44.0  0.00    48.0  0.00  
# WL H  MD  W
    53.33 0.0  

<STA_COORD>  0.0   # Station 0
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    2.76  0.0     2.76  0.94    2.76  0.94    3.0   1.51    4.0   2.75  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   3.25    6.0   3.49    8.0   3.72    10.0  3.65    12.0  3.31
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  2.8     20.0  1.34    22.28 0.83    26.0  0.36    28.36 0.02  
# WL H      W      H      W      H      W      H      W      H      W
    34.0  0.00    37.1  0.00    40.0  0.00    44.0  0.00    48.0  0.00  
# WL H  MD  W
    53.33 0.17 

<STA_COORD>  28.3  # Station 1
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   4.23    3.0   5.08    4.0   5.59  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   6.09    6.0   6.39    8.0   6.88    10.0  7.07    12.0  7.13  
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  7.01    20.0  6.74    22.28 6.44    26.0  6.18    28.36 6.14  
# WL H      W      H      W      H      W      H      W      H      W
    34.0  6.55    37.1  7.38    40.0  8.55    44.0  10.65   48.0  13.27 
# WL H  MD  W
    52.70 16.95

<STA_COORD>  56.6  # Sataion 2
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   6.41    3.0   7.94    4.0   9.23  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   10.08   6.0   10.78   8.0   11.86   10.0  12.69   12.0  13.20 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  13.78   20.0  14.00   22.28 14.02   26.0  14.04   28.36 14.07 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  14.60   37.1  15.59   40.0  16.85   44.0  19.07   48.0  21.63 
# WL H  MD  W
    52.22 24.30

<STA_COORD>  84.9  # Station 3
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   9.71    3.0   12.0    4.0   13.75 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   15.3    6.0   16.42   8.0   18.23   10.0  19.51   12.0  20.47 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  21.69   20.0  22.49   22.28 22.71   26.0  22.76   28.36 22.76 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  23.08   37.1  23.51   40.0  24.41   44.0  27.28   48.0  28.74 
# WL H  MD  W
    51.72 30.91

<STA_COORD> 113.2  # Station 4
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   14.72   3.0   17.81   4.0   20.27
# WL H      W      H      W      H      W      H      W      H      W
    5.0   22.07   6.0   23.51   8.0   25.69   10.0  27.13  12.0  28.30 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  29.63   20.0  30.43   22.28 30.50   26.0  30.59   28.36 30.65 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  30.81   37.1  31.07   40.0  31.45   44.0  32.76   48.0  34.26 
# WL H  MD  W
    51.16 35.58

<STA_COORD> 141.5  # Station 5
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   21.23   3.0   24.91   4.0   27.94 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   30.06   6.0   31.55   8.0   33.63   10.0  35.02   12.0  36.08 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  37.20   20.0  37.79   22.28 37.84   26.0  37.74   28.36 37.68 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  37.52   37.1  37.42   40.0  37.26   44.0  38.00   48.0  38.58 
# WL H  MD  W
    50.64 39.20

<STA_COORD> 169.8  # Station 6
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   28.78   3.0   31.89   4.0   34.40 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   35.98   6.0   37.20   8.0   38.75   10.0  39.82   12.0  40.72 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  41.68   20.0  42.05   22.28 42.05   26.0  41.89   28.36 41.79 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  41.52   37.1  41.47   40.0  41.31   44.0  41.53   48.0  41.61 
# WL H  MD  W
    50.26 41.51

<STA_COORD> 198.1  # Station 7
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   35.52   3.0   37.54   4.0   38.77 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   39.71   6.0   40.45   8.0   41.84   10.0  42.64   12.0  43.87 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  44.03   20.0  44.35   22.28 44.35   26.0  43.87   28.36 43.60 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  42.85   37.1  42.53   40.0  42.16   44.0  42.24   48.0  42.50 
# WL H  MD  W
    49.82 42.50

<STA_COORD> 226.4  # Station 8
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   38.50   3.0   40.53   4.0   42.02 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   42.91   6.0   43.65   8.0   44.83   10.0  45.52   12.0  46.00 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  46.42   20.0  46.42   22.28 46.42   26.0  45.73   28.36 45.31 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  44.19   37.1  43.65   40.0  43.07   44.0  43.17   48.0  43.45 
# WL H  MD  W
    49.32 43.45

<STA_COORD> 254.7  # Station 9
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   41.12   3.0   42.56   4.0   43.52 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   44.24   6.0   44.93   8.0   45.89   10.0  46.42   12.0  46.85 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  47.12   20.0  47.12   22.28 47.12   26.0  46.26   28.36 45.73 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  44.61   37.1  43.97   40.0  43.39   44.0  43.43   48.0  43.90 
# WL H  MD  W
    49.00 43.90

<STA_COORD> 283.0  # Station 10
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   41.84   3.0   43.23   4.0   44.24 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   44.99   6.0   45.68   8.0   46.48   10.0  47.01   12.0  47.44 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  47.38   20.0  47.44   22.28 47.44   26.0  46.96   28.36 46.80 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  45.94   37.1  45.57   40.0  45.15   44.0  44.00   48.0  44.00 
# WL H  MD  W
    48.65 44.00

<STA_COORD> 311.3  # Station 11
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   41.84   3.0   43.23   4.0   44.24 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   44.99   6.0   45.68   8.0   46.48   10.0  47.01   12.0  47.44 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  47.38   20.0  47.44   22.28 47.44   26.0  46.96   28.36 46.80 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  45.94   37.1  45.57   40.0  45.15   44.0  44.00   48.0  44.00 
# WL H  MD  W
    48.20 44.00

<STA_COORD> 339.6  # Station 12
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   40.13   3.0   42.05   4.0   43.44 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   44.51   6.0   45.46   8.0   46.48   10.0  47.01   12.0  47.44 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  47.38   20.0  47.44   22.28 47.44   26.0  46.96   28.36 46.80 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  45.94   37.1  45.57   40.0  45.15   44.0  44.83   47.89 44.70 
# WL H  MD  W
    47.89 44.70

<STA_COORD> 367.9  # Station 13
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   32.62   3.0   36.08   4.0   38.80 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   40.77   6.0   42.21   8.0   44.35   10.0  45.73   12.0  46.48 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  46.96   20.0  47.06   22.28 47.06   26.0  46.64   28.36 46.42 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  45.68   37.1  45.36   40.0  44.99   44.0  44.72   47.52 44.72 
# WL H  MD  W
    47.52 44.72

<STA_COORD> 396.2  # Station 14
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   23.13   3.0   27.77   4.0   31.13 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   33.79   6.0   36.03   8.0   39.34   10.0  41.79   12.0  43.55 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  45.68   20.0  46.16   22.28 46.16   26.0  45.89   28.36 45.68 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  45.15   37.1  44.93   40.0  44.61   44.0  44.25   47.19 44.25 
# WL H  MD  W
    47.19 44.25

<STA_COORD> 424.5  # Station 15
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   13.32   3.0   17.00   4.0   20.52 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   23.99   6.0   27.08   8.0   32.30   10.0  36.08   12.0  38.80 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  42.00   20.0  43.39   22.28 43.44   26.0  43.33   28.36 43.28 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  43.12	  37.1  43.05   40.0  42.96   44.0  42.92   46.81 42.92 
# WL H  MD  W
    46.81 42.92

<STA_COORD> 452.8  # Station 16
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   7.25    3.0   9.97    4.0   12.26 
# WL H      W      H      W      H      W      H      W      H      W
    5.0   14.60   6.0   16.84   8.0   20.79   10.0  24.68   12.0  28.25 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  33.53   20.0  36.24   22.28 37.26   26.0  38.32   28.36 38.75 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  39.55   37.1  39.92   40.0  40.10   44.0  40.35   46.53 40.35 
# WL H  MD  W
    46.53 40.35

<STA_COORD> 481.1  # Station 17
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   1.5     2.0   3.57    3.0   4.69    4.0   5.81  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   7.09    6.0   8.42    8.0   11.19   10.0  14.07   12.0  16.95 
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  22.01   20.0  26.29   22.28 28.14   26.0  30.91   28.36 32.25 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  34.59   37.1  35.39   40.0  35.82   44.0  36.00   46.28 36.00 
# WL H  MD  W
    46.28 36.00

<STA_COORD> 509.4  # Station 18
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   2.33    2.0   1.87    3.0   2.03    4.0   2.29  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   2.67    6.0   3.14    8.0   4.21    10.0  5.81    12.0  7.73  
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  12.21   20.0  17.11   22.28 19.83   26.0  23.40   28.36 25.16 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  28.20   37.1  29.00   40.0  29.32   44.0  29.38   46.11 29.38 
# WL H  MD  W
    46.11 29.38

<STA_COORD> 537.7  # Station 19
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   7.25    2.0   7.25    3.0   7.25    4.0   7.25  
# WL H      W      H      W      H      W      H      W      H      W
    5.0   7.25    6.0   7.25    8.0   1.53    10.0  1.72    12.0  2.15  
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  3.68    20.0  6.5     22.28 8.69    26.0  12.10   28.36 14.08 
# WL H      W      H      W      H      W      H      W      H      W
    34.0  17.01   37.1  17.81   40.0  18.25   44.0  18.49   45.95 18.49 
# WL H  MD  W
    45.95 18.49

<STA_COORD> 566.0  # Station 20
<STK_COORDS> 
# WL H  CL  W      H  HS  W      H      W      H      W      H      W
    0.0   0.0     0.0   0.0     2.0   0.0     3.0   0.0     4.0   0.0   
# WL H      W      H      W      H      W      H      W      H      W
    5.0   0.0     6.0   0.0     8.0   0.0     10.0  0.0     12.0  0.0   
# WL H      W      H      W      H PDK  W      H      W      H DWL  W
    16.0  0.0     20.0  0.00    22.28 0.00    26.0  0.00    28.36 0.00  
# WL H      W      H      W      H      W      H      W      H      W
    34.0  0.00    37.1  0.00    40.0  0.00    44.0  0.00    45.85 0.00  
# WL H  MD  W
    45.85 0.00 
<EOF> 

